﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.Web.Script.Serialization;

namespace RecoveryBus
{   
    public partial class Form1 : Form
    {
        static Dictionary<string, ManagementObject> GetPnPDevices()
        {
            Dictionary<string, ManagementObject> pnpDevices = new Dictionary<string, ManagementObject>();

            ManagementObjectCollection collection;

            using (var searcher = new ManagementObjectSearcher(@"Select * From Win32_PnPEntity"))
                collection = searcher.Get();

            foreach (var device in collection)
            {
                pnpDevices.Add((string)device.GetPropertyValue("PNPDeviceID"), (ManagementObject)device);
            }

            return pnpDevices;
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Checking devices.");
            var pnpDevices = GetPnPDevices();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Completely erase disk ", "Proceed?", MessageBoxButtons.OKCancel);

            if(res == DialogResult.OK)
            {
                MessageBox.Show("Proceeding.");
            }
            else
            {
                MessageBox.Show("Cancelling.");
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
